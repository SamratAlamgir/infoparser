﻿using System;

namespace InfoParser
{
    class Program
    {
        static void Main(string[] args)
        {
            PersonFileGenerator personFileGenerator = new PersonFileGenerator();
            personFileGenerator.GeneratePersonInfoFiles();

            Console.ReadKey();
        }
    }
}
