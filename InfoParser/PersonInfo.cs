﻿namespace InfoParser
{
    public class PersonInfo
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Address { get; set; }
        public string StreetName { get; set; }
        public string PhoneNumber { get; set; }

        public static PersonInfo Parse(string infoStr)
        {
            string[] values = infoStr.Split(',');

            PersonInfo person = new PersonInfo
            {
                FirstName = values[0],
                LastName = values[1],
                Address = values[2],
                PhoneNumber = values[3]
            };

            int streetNameStartIndex = person.Address != null ? (person.Address.IndexOf(' ') + 1) : -1;
            person.StreetName = streetNameStartIndex != -1 ? person.Address.Substring(streetNameStartIndex, person.Address.Length - streetNameStartIndex) : person.Address;

            return person;
        }
    }
}
