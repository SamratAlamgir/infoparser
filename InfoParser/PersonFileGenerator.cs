﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InfoParser
{
    public class PersonFileGenerator
    {
        Properties.Settings settings = Properties.Settings.Default;

        public List<PersonInfo> GeneratePersonInfoList()
        {
            List<PersonInfo> values = null;
            try
            {
                values = File.ReadAllLines(settings.SourceFilePath)
                                          .Skip(1)
                                          .Select(p => PersonInfo.Parse(p))
                                          .ToList();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error on file parsing...\n");
                Console.WriteLine(ex.Message);
            }

            return values;
        }

        public string GetNameFrequencyStr(List<PersonInfo> personInfoList)
        {
            var names = (from person in personInfoList
                         select person.FirstName)
                        .Concat(from person in personInfoList
                                select person.LastName);

            var result = (from name in names
                          group name by name into g
                          select new { Frequency = g.Count(), Name = g.Key } into r
                          orderby r.Frequency, r.Name
                          select r).ToList();

            var sb = new StringBuilder();

            foreach (var r in result)
            {
                if (sb.Length > 0)
                {
                    sb.AppendLine();
                }

                sb.Append(r.Frequency).Append(",").Append(r.Name);
            }

            return sb.ToString();
        }

        public string GetAddressesStr(List<PersonInfo> personInfoList)
        {
            var result = (from person in personInfoList
                          orderby person.StreetName
                          select person.Address).ToList();

            var sb = new StringBuilder();

            foreach (var r in result)
            {
                if (sb.Length > 0)
                {
                    sb.AppendLine();
                }

                sb.Append(r);
            }
            return sb.ToString();
        }

        public void GeneratePersonInfoFiles()
        {
            var personList = GeneratePersonInfoList();

            try
            {
                if (personList != null)
                {
                    WriteToFile(settings.NameInfoFilePath, GetNameFrequencyStr(personList));
                    WriteToFile(settings.AddressInfoFilePath, GetAddressesStr(personList));
                    Console.WriteLine("Files are generated...");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error on generating file...\n");
                Console.WriteLine(ex.Message);
            }
        }

        public void WriteToFile(string filePath, string contents)
        {
            Directory.CreateDirectory(Path.GetDirectoryName(filePath));

            File.WriteAllText(filePath, contents);
        }
    }
}
