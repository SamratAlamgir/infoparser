﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;

namespace InfoParser.UnitTest
{
    [TestClass]
    public class InfoParserTest
    {
        [TestMethod]
        public void TestGetNameFrequencyStr()
        {
            // arrange
            var pInfoList = new List<PersonInfo>();

            pInfoList.Add(new PersonInfo
            {
                FirstName = "Will",
                LastName = "Smith",
                Address = "8 Crimson Rd",
                StreetName = "Crimson Rd",
                PhoneNumber = "31214788"
            });

            pInfoList.Add(new PersonInfo
            {
                FirstName = "Johny",
                LastName = "Depp",
                Address = "82 Stewart St",
                StreetName = "Stewart St",
                PhoneNumber = "31256288"
            });

            pInfoList.Add(new PersonInfo
            {
                FirstName = "James",
                LastName = "Owen",
                Address = "78 Short Lane",
                StreetName = "Short Lane",
                PhoneNumber = "31213264"
            });
            pInfoList.Add(new PersonInfo
            {
                FirstName = "Clive",
                LastName = "Owen",
                Address = "65 Ambling Way",
                StreetName = "Ambling Way",
                PhoneNumber = "31326788"
            });

            pInfoList.Add(new PersonInfo
            {
                FirstName = "Jimmy",
                LastName = "Smith",
                Address = "94 Roland St",
                StreetName = "Roland St",
                PhoneNumber = "31212488"
            });

            // act
            PersonFileGenerator pfg = new PersonFileGenerator();
            string nameStr = pfg.GetNameFrequencyStr(pInfoList);

            // assert
            Assert.AreEqual("1,Clive\r\n1,Depp\r\n1,James\r\n1,Jimmy\r\n1,Johny\r\n1,Will\r\n2,Owen\r\n2,Smith", nameStr);
        }

        [TestMethod]
        public void TestGetAddressesStr()
        {
            // arrange
            var pInfoList = new List<PersonInfo>();

            pInfoList.Add(new PersonInfo
            {
                FirstName = "Will",
                LastName = "Smith",
                Address = "8 Crimson Rd",
                StreetName = "Crimson Rd",
                PhoneNumber = "31214788"
            });

            pInfoList.Add(new PersonInfo
            {
                FirstName = "Johny",
                LastName = "Depp",
                Address = "82 Stewart St",
                StreetName = "Stewart St",
                PhoneNumber = "31256288"
            });

            pInfoList.Add(new PersonInfo
            {
                FirstName = "James",
                LastName = "Owen",
                Address = "78 Short Lane",
                StreetName = "Short Lane",
                PhoneNumber = "31213264"
            });
            pInfoList.Add(new PersonInfo
            {
                FirstName = "Clive",
                LastName = "Owen",
                Address = "65 Ambling Way",
                StreetName = "Ambling Way",
                PhoneNumber = "31326788"
            });

            pInfoList.Add(new PersonInfo
            {
                FirstName = "Jimmy",
                LastName = "Smith",
                Address = "94 Roland St",
                StreetName = "Roland St",
                PhoneNumber = "31212488"
            });

            // act
            PersonFileGenerator pfg = new PersonFileGenerator();
            string addressStr = pfg.GetAddressesStr(pInfoList);

            // assert
            Assert.AreEqual("65 Ambling Way\r\n8 Crimson Rd\r\n94 Roland St\r\n78 Short Lane\r\n82 Stewart St", addressStr);
        }
    }
}
